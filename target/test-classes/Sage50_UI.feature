#Author: susmita
Feature: Verify the Sage50 UI 
#Verify the UI of Sage50 Marketing Home page-1
@VerifyUIofMarketingPage
 Scenario: Verify the UI of 'Marketing page'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on 'Start Session' button
 Then Verify the UI Element of Marketing page
 
#Verify the UI of Sage50 Get Started-Screen1
@VerifyUIofGetStarted-Screen1
 Scenario: Verify the UI of Get Started-Screen1
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 Then Verify the UI of GetStarted-Screen1 page
 
#Verify the UI of Sage50 Get Started-Screen2
@VerifyUIofGetStarted-Screen2
 Scenario: Verify the UI of Get Started-Screen2
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 Given Enter amt in EMCC sales count
 And Click on 'View Offers' button
 Then Verify the text message of GetStarted-Screen2 page
 And Verify the 'Cancel' and 'Next' button on GetStarted-Screen2 page
 
#Verify the UI of Sage50 'Business Information' 
@VerifyUIofBusinessInformation
 Scenario: Verify the UI of 'Business Information'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Then Verify the textbox and dropdown Elements of 'Business Information' page
 And Verify the 'Back', 'Cancel' and 'Next' button on 'Business Information' page
 
#Verify the UI of Sage50 'Personal Information'
@VerifyUIofPersonalInformation'
 Scenario: Verify the UI of 'Personal Information'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the data in all mandatory fields 'Business Information' page
 When Click on 'Next2' button
 Then Verify the textbox and dropdown Elements of 'Personal Information' page
 And Verify the 'Back', 'Cancel' and 'Next' button on 'Personal Information' page
  
 #Verify the UI of Sage50 'Personal Information'
@VerifySSNTextBox
 Scenario Outline: Verify the UI of 'Personal Information'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the data in all mandatory fields and Select select Ownership Type as <ownership> 'Business Information' page
 When Click on 'Next2' button
 Then Verify the SSN text box in 'Personal Information' page
 
 Examples:
 | ID       |Prod                       |Amt|ownership           |
 |1234567890|SAGEPEAC2011000ACCTAAC1USEN|50 |Sole Proprietor     |
 |1234567890|SAGEPEAC2011000ACCTAAC1USEN|50 |LLC- Sole Proprietor|
 
 
   
 #Verify the UI of Sage50 'Personal Information'
@VerifyFuncOfUseBusinessAddressCheckBox
 Scenario: Verify the UI of 'Personal Information'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter fields details with Business add is 10DownStreet,City is California, State US and Zip is 32456 in Business Information' page
 When Click on 'Next2' button
 And Click on 'Use Business address' Checkbox
 Then Verify the Address details which is same as Business add is 10DownStreet,City is California, State US and Zip is 32456
 
 #Verify the UI of Sage50 'Personal Information'
@VerifyFuncOfUseBusinessAddressCheckBox
 Scenario: Verify the UI of 'Personal Information'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 Then Verify the 'Personal Information' page message.
 Given Enter all the mandatory fields details in 'Personal Information' page
 When Click on 'Next3' button
 And Click on 'Use Business address' Checkbox
 Then Verify the Address details which is same as Business add is 10DownStreet,City is California, State US and Zip is 32456
 
 
 #Verify the UI of Sage50 'Billing and Sales' 
@VerifyUIOfBillingandSales
 Scenario: Verify the UI of 'Personal Information'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 Then Verify the 'Personal Information' page message.
 Given Enter all the mandatory fields details in 'Personal Information' page
 When Click on 'Next3' button
 And Click on 'Use Business address' Checkbox
 And Click on 'Next4' button
 Then Verify the UI elmeents of Billing and Sales page
 

 #Verify the UI of Sage50 'Review and Sign Your Application' 
@VerifyUIOfReviewAndSignYourApplication
 Scenario: Verify the UI of 'Review and Sign Your Application'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 Then Verify the 'Personal Information' page message.
 Given Enter all the mandatory fields details in 'Personal Information' page
 When Click on 'Next3' button
 And Click on 'Use Business address' Checkbox
 And Click on 'Next4' button
 Then Verify the UI elmeents of Review and Sign Your Application page
 
  
 
 #Verify the UI of Sage50 'Review and Sign Your Application' if Do you have an alternate business address? For example, DBA location is different?
 #checkbox is checked in Business Information Page.
  
@VerifyUIOfReviewAndSignYourApplicationIfAlternateAddressCheckboxChecked
 Scenario: Verify the UI of 'Review and Sign Your Application' If Do you have AlternateAddress Checkbox is Checked
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 Then Verify the 'Personal Information' page message.
 Given Enter all the mandatory fields details in 'Personal Information' page
 When Click on 'Next3' button
 And Click on 'Use Business address' Checkbox
 And Click on 'Next4' button
 Then Verify the UI elmeents of Review and Sign Your Application page
 
 #Verify tooltip of Bank Routing Number in Business Information screen
 @VerifyTooltipBankRoutingNumber
 Scenario: Verify the tooltip in 'Business Information' screen
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 And Click on 'Next2' button
 And Click on '?' icon of BRN
 Then Verify the Tooltips of BRN
 When Click on '?' icon of EIN
 Then Verify the Tooltips of EIN
 
 #Verify the "Header" elements
 @VerifyHeaderElements
 Scenario: Verify tooltip in 'Business Information' screen
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button 
 Then Verify the Header Elements
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 Then Verify the Header Elements
 When Click on 'Next1' button
 Then Verify the Header Elements
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 Then Verify the Header Elements
 Given Enter all the mandatory fields details in 'Personal Information' page
 When Click on 'Next3' button
 Then Verify the Header Elements
 When Click on 'Use Business address' Checkbox
 And Click on 'Next4' button
 Then Verify the Header Elements
 
 
 #Verify the "Footer" elements
 @VerifyFooterElements
 Scenario: Verify tooltip in all the screens
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button 
 Then Verify the Footer Elements
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 Then Verify the Footer Elements
 When Click on 'Next1' button
 Then Verify the Footer Elements
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 Then Verify the Footer Elements
 Given Enter all the mandatory fields details in 'Personal Information' page
 When Click on 'Next3' button
 Then Verify the Footer Elements
 When Click on 'Use Business address' Checkbox
 And Click on 'Next4' button
 Then Verify the Footer Elements
 
 
 #Verify the 'Disclaimer' text
 @VerifyDisclaimerText
 Scenario: Verify the text of 'Disclaimer'  in 'Getting Started' Screen-2
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button 
 Then Verify the Footer Elements
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Disclaimer' link
 Then Verify the Disclaimer text
 
 
 #Verify the Tooltip of "Last 4 Digits of Your SSN"
 @VerifyLast4DigitsOfYourSSN
 Scenario: Verify the Tooltip of "Last 4 Digits of Your SSN"
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 Then Verify the 'Personal Information' page message.
 Given Enter all the mandatory fields details in 'Personal Information' page
 When Click on 'Next3' button
 And Click on 'Use Business address' Checkbox
 And Click on 'Next4' button 
 And Click on 'Sign Your Application'
 And Click on '?' icon of 'Last 4 Digits of Your SSN'
 Then Verify the tooltip of 'Last 4 Digits of Your SSN'
 When Click on '?' icon of 'Your Date of Birth'
 Then Verify the tooltip of 'Your Date of Birth'
 
 
 #Verify the Submitted success message
 @VerifySubmittedSuccessMessage
 Scenario: Verify the Tooltip of "Last 4 Digits of Your SSN"
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 Then Verify the 'Personal Information' page message.
 Given Enter all the mandatory fields details in 'Personal Information' page
 When Click on 'Next3' button
 And Click on 'Use Business address' Checkbox
 And Click on 'Next4' button 
 And Click on 'Sign Your Application'
 And Click on the PDFs Links
 And Click on 'Terms&Condition' check box
 Given Enter 'SSN' and 'DOB'
 When Click on 'Submit Application' button
 Then Verify the Submission Success Message 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 