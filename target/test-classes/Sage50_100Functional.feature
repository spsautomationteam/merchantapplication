#Author: Susmita

Feature: Verify the Sage50 UI 
#Verify the UI of Sage50 Marketing Home page-1
@VerifyUIofMarketingPage
 Scenario: Verify the UI of 'Marketing page'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on 'Start Session' button
 Then Verify the 'Get Started Today' Page
 
 
@VerifyAppSubmissionSuccessMsg
 Scenario: Verify the application submission message
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 Then Verify the 'Personal Information' page message.
 Given Enter all the mandatory fields details in 'Personal Information' page
 When Click on 'Next3' button
 And Click on 'Use Business address' Checkbox
 And Click on 'Next4' button
 And Click on 'Sign Your Application'
 And Click on the PDFs Links
 And Click on 'Terms&Condition' check box
 Given Enter 'SSN' and 'DOB'
 When Click on 'Submit Application' button
 Then Verify the Submission Success Message 

@VerifyBackButtonFuncInBusinessInfoScreen
 Scenario: Verify the application submission message
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 And Click on 'Back1' button
 Then Verify the Screen should be as Getting Started Screen1

 
 @VerifyBackButtonFuncInPersonalInfoScreen
 Scenario: Verify the application submission message
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 And Click on 'Back2' button
 Then Verify the Screen should be as 'Getting Started Screen-2' page
 
 
 
  @VerifyBackButtonFuncInPersonalInfoScreen
 Scenario: Verify the application submission message
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on Start Session button
 And Click on 'Get Started Today!' button
 Given Enter EMCC sales count amount <Amt>
 When Select CCard Persentage
 And Click on 'View Offers' button
 And Click on 'Next1' button
 Given Enter all the mandatory fields details in 'Business Information' page
 When Click on 'Next2' button
 Then Verify the 'Personal Information' page message.
 Given Enter all the mandatory fields details in 'Personal Information' page
 When Click on 'Next3' button
 And Click on the 'Back3' button
 Then Verify the Screen should be as 'Business Information' page
 
 
 
 
 