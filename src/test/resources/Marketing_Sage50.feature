#Author: susmita
Feature: Verify the Sage50_Merchant Page

#Verify the UI of Sage50 Marketing Home page-1
@VerifyUIofMarketingPage
 Scenario: Verify the UI of 'Marketing page'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on 'Start Session' button
 Then Verify the UI Element of Marketing page


#Verify the UI of Sage50 Marketing Home page-1
@VerifyUIofMarketingPage
 Scenario: Verify the UI of 'Marketing page'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on 'Start Session' button
 Then Verify text and position of 'Get Started Today!' button 
 
 
#Verify the Chat Link of Sage50 Marketing Home page-1
@VerifyTheChatLinkText
 Scenario: Verify the UI of 'Marketing page'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on 'Start Session' button
 Then Verify the text of  'Chat' link
 
 
 #Verify the ChatLink
@ValidateChatLink
 Scenario: Verify the UI of 'Marketing page'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on 'Start Session' button
 And Click on 'Chat now with one of our representatives' link
 Then Verify the Guest Name: in  'Sage Payment Live Chat' window
 
 #Verify the successfully login to Sage Payment Live Window
@ValidateLoginInChatWindow
 Scenario: Verify the UI of 'Marketing page'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on 'Start Session' button
 And Click on 'Chat now with one of our representatives' link
 Then Verify the Guest Name: in  'Sage Payment Live Chat' window
 Given Enter the Guest Name as Test
 When Click on the 'SihnIn' button
 Then Verify the text as 'Your chat is being routed to the next available agent.'
 
 
 #Verify the Chat Link of Sage50 Marketing Home page-1
@VerifyTheChatLinkText
 Scenario: Verify the UI of 'Marketing page'
 Given Enter BPID <ID> and ProductID <Prod ID>
 When Click on 'Start Session' button
 Then Verify the text of 'Call Us' Text with number
 
 
 
 
 
 
 
 
 
 
 
 
 
 